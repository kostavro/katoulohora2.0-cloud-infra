locals {
  account_name   = "prod"
  aws_account_id = "235470632765"
  aws_profile    = "prod"
  iam_role       = "arn:aws:iam::235470632765:role/admin"
}