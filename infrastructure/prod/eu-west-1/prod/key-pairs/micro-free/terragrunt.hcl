# -------------------------------------------------------
# Key pair for the t3.micro EC2 instance
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-key-pair?ref=v1.0.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  key_name   = "micro-free"
  public_key = file("id_rsa_aws_xyz_ec2.pub")
}