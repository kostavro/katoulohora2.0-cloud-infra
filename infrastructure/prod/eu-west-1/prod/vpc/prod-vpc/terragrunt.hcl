# -------------------------------------------------------
# The main VPC
# -------------------------------------------------------
#checkov:skip=CKV2_AWS_11: No flow logs
#checkov:skip=CKV2_AWS_19: EIPs are not allocated
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-vpc?ref=v3.6.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = "prod-vpc"
  cidr = "10.0.0.0/16"
  private_subnets = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",    
  ]
  public_subnets = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24"
  ]
  database_subnets = [
    "10.0.81.0/24",
    "10.0.82.0/24",
    "10.0.83.0/24"
  ]
  create_elasticache_subnet_group = false
  create_redshift_subnet_group = false

  enable_dns_hostnames = true
  enable_nat_gateway = true
  single_nat_gateway = true
  one_nat_gateway_per_az = false
  
  azs = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]

  map_public_ip_on_launch = false

  manage_default_security_group = true
  default_security_group_ingress = []
  default_security_group_egress  = []
}