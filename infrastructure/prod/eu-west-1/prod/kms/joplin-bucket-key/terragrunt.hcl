# -------------------------------------------------------
# KMS key to encrypt Joplin S3
# -------------------------------------------------------
#checkov:skip=CKV_AWS_7: No rotation
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:cloudposse/terraform-aws-kms-key?ref=0.11.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  description         = "KMS key to encrypt Joplin S3"
  enable_key_rotation = false
  enabled             = true
  name                = "joplin-bucket-key"
}