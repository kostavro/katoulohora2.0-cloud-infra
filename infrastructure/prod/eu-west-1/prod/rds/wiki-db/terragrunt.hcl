# -------------------------------------------------------
# Wiki.js database
# -------------------------------------------------------
#checkov:skip=CKV_AWS_157: No multi-AZ
#checkov:skip=CKV_AWS_161: No IAM authentication
#checkov:skip=CKV_AWS_16: db.t2.micro can't be encrypted
#checkov:skip=CKV_AWS_129: No logs
#checkov:skip=CKV_AWS_118: No enhanced monitoring
#checkov:skip=CKV2_AWS_27: No query loggin
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-rds?ref=v3.4.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

dependency "sg" {
  config_path = "../../security-groups/ingress-postgres"
}

inputs = {
  identifier                  = "wiki-db"
  port                        = 5432

  name                        = "wikidatabase"
  username                    = "wikidatabase_master"
  password                    = get_env("TF_VAR_WIKI_DB_PASS", "export TF_VAR_WIKI_DB_PASS variable with value")

  allocated_storage           = "18"
  max_allocated_storage       = "20"
  storage_encrypted           = false
  storage_type                = "gp2"

  allow_major_version_upgrade = true
  engine                      = "postgres"
  engine_version              = "12.3"
  family                      = "postgres12"
  instance_class              = "db.t2.micro"

  availability_zone           = "eu-west-1a"
  subnet_ids                  = dependency.vpc.outputs.database_subnets
  vpc_security_group_ids      = [dependency.sg.outputs.security_group_id]

  backup_retention_period     = 7
  backup_window               = "02:00-02:45"
  maintenance_window          = "Mon:04:00-Mon:06:00"
}