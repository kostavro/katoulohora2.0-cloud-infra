# -------------------------------------------------------
# LB for micro-free wiki.js ASG
# -------------------------------------------------------
#checkov:skip=CKV_AWS_91: No access logs
#checkov:skip=CKV2_AWS_28: No WAF
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-alb?ref=v6.5.0"
}

include {
  path = find_in_parent_folders()
}

dependency "ssh_sg" {
  config_path = "../../security-groups/allow-ssh"
}

dependency "egress_sg" {
  config_path = "../../security-groups/egress-all"
}

dependency "http_sg" {
  config_path = "../../security-groups/allow-http"
}

dependency "https_sg" {
  config_path = "../../security-groups/allow-https"
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

inputs = {
  name                       = "micro-free-lb"
  enable_deletion_protection = true
  idle_timeout                = "300"

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
      action_type        = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]
  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      target_group_index = 0
      certificate_arn    = "arn:aws:acm:eu-west-1:235470632765:certificate/a99b80c7-3c20-4b3e-a79f-a0cd08a7ef77" # Manually imported into ACM
    }
  ]
  listener_ssl_policy_default = "ELBSecurityPolicy-TLS-1-2-2017-01"
  drop_invalid_header_fields  = true

  target_groups = [
    {
      name_prefix      = "ec2-"
      backend_protocol = "HTTP"
      backend_port     = 3000
      target_type      = "instance"
    }
  ]

  vpc_id  = dependency.vpc.outputs.vpc_id
  subnets = dependency.vpc.outputs.public_subnets
  security_groups = [
    dependency.egress_sg.outputs.security_group_id,
    dependency.http_sg.outputs.security_group_id,
    dependency.https_sg.outputs.security_group_id,
  ]
}