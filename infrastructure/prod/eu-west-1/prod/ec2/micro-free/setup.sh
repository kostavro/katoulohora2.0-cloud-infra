#!/bin/bash

###############
##### SSH #####
###############

# SSH: forbid root, empty passwords, close stale connections, change port, disable x11 forwarding
sed 's/#PermitRootLogin yes/PermitRootLogin no/g' -i /etc/ssh/sshd_config
sed 's/#ClientAliveInterval 0/ClientAliveInterval 120/g' -i /etc/ssh/sshd_config
sed 's/#ClientAliveCountMax 3/ClientAliveCountMax 3/g' -i /etc/ssh/sshd_config
sed 's/#Port 22/Port 22222/g' -i /etc/ssh/sshd_config
sed 's/X11Forwarding yes/X11Forwarding no/g' -i /etc/ssh/sshd_config

# Install fail2ban
sudo amazon-linux-extras install epel -y
yum -y install fail2ban
systemctl enable fail2ban
systemctl start fail2ban

# Configure fail2ban for sshd
touch /etc/fail2ban/jail.d/sshd.local
echo "[sshd]
enabled = true
port = ssh
action = iptables-multiport
logpath = /var/log/secure
maxretry = 3
bantime = 600" > /etc/fail2ban/jail.d/sshd.local

# SSHd restart
sudo systemctl restart sshd


###################
##### Wiki.js #####
###################

# Install NodeJS
yum install -y gcc-c++ make 
sudo -u ec2-user bash -c 'curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash -'
yum install -y nodejs

# Install Wiki.js
sudo -u ec2-user bash -c 'cd /home/ec2-user && wget https://github.com/Requarks/wiki/releases/download/2.5.214/wiki-js.tar.gz'
sudo -u ec2-user bash -c 'cd /home/ec2-user && mkdir wiki'
sudo -u ec2-user bash -c 'cd /home/ec2-user && tar xzf wiki-js.tar.gz -C ./wiki'
sudo -u ec2-user bash -c 'cd /home/ec2-user && cd ./wiki'
sudo -u ec2-user bash -c 'cd /home/ec2-user/wiki && mv config.sample.yml config.yml'
# DB
sudo -u ec2-user bash -c 'cd /home/ec2-user/wiki && sed "s/host: localhost/host: ${db_host}/g" -i config.yml'
sudo -u ec2-user bash -c 'cd /home/ec2-user/wiki && sed "s/user: wikijs/user: ${db_user}/g" -i config.yml'
sudo -u ec2-user bash -c 'cd /home/ec2-user/wiki && sed "s/pass: wikijsrocks/pass: ${db_pass}/g" -i config.yml'
sudo -u ec2-user bash -c 'cd /home/ec2-user/wiki && sed "s/db: wiki/db: ${db_name}/g" -i config.yml'

# Set up systemd service
echo "[Unit]
Description=Wiki.js
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/node server
Restart=always
User=ec2-user
Environment=NODE_ENV=production
WorkingDirectory=/home/ec2-user/wiki

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/wiki.service

systemctl daemon-reload
systemctl enable wiki
systemctl start wiki
