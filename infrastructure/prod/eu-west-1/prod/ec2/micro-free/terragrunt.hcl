# -----------------------------------------------------------------
# ASG with 1 t3.micro of Free Tier - will expire mid-Summer 2022
# -----------------------------------------------------------------
#checkov:skip=CKV_AWS_88: I want to have a public IP
#checkov:skip=CKV_AWS_79: Instance Metadata v1 enabled
#checkov:skip=CKV_AWS_126: No detailed monitoring
# -----------------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-autoscaling?ref=v4.6.0"
}

include {
  path = find_in_parent_folders()
}

dependency "key_pair" {
  config_path = "../../key-pairs/micro-free"
}

dependency "ssh_sg" {
  config_path = "../../security-groups/allow-ssh"
}

dependency "egress_sg" {
  config_path = "../../security-groups/egress-all"
}

dependency "http_three_sg" {
  config_path = "../../security-groups/allow-3000"
}

dependency "https_three_sg" {
  config_path = "../../security-groups/allow-3443"
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

dependency "rds" {
  config_path = "../../rds/wiki-db"
}

dependency "alb" {
  config_path = "../../load-balancers/micro-free-lb"
}

inputs = {
  create_lc              = true
  use_lc                 = true
  lc_name                = "micro-free-lc"
  desired_capacity       = 1
  min_size               = 0
  max_size               = 1

  description            = "t3.micro instance with Wiki.js"
  name                   = "micro-free"
  instance_type          = "t3.micro"
  image_id               = "ami-0d1bf5b68307103c2" # Amazon Linux FreeTier
  key_name               = dependency.key_pair.outputs.key_pair_key_name

  health_check_type      = "EC2"
  target_group_arns      = dependency.alb.outputs.target_group_arns
  associate_public_ip_address = true
  security_groups = [
    dependency.ssh_sg.outputs.security_group_id,
    dependency.egress_sg.outputs.security_group_id,
    dependency.http_three_sg.outputs.security_group_id,
    dependency.https_three_sg.outputs.security_group_id
  ]
  vpc_zone_identifier = dependency.vpc.outputs.public_subnets

  user_data = templatefile(
    "setup.sh", {
      db_host = "${dependency.rds.outputs.db_instance_address}",
      db_port = "${dependency.rds.outputs.db_instance_port}",
      db_user = "${dependency.rds.outputs.db_instance_username}",
      db_pass = "${dependency.rds.outputs.db_instance_password}",
      db_name = "${dependency.rds.outputs.db_instance_name}",
    }
  )

  ebs_optimized = true
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      volume_size = 25
    }
  ]
}