# -------------------------------------------------------
# Allow incoming on port 3443
# -------------------------------------------------------
#checkov:skip=CKV2_AWS_5: Attach to micro-free EC2
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-security-group?ref=v4.3.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

inputs = {
  name = "allow-3443"
  description = "Allow incoming on 3443"
  vpc_id = dependency.vpc.outputs.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 3443
      to_port     = 3443
      protocol    = "tcp"
      description = "node.js https"
      cidr_blocks = join(",", dependency.vpc.outputs.public_subnets_cidr_blocks)
    }  
  ]
}