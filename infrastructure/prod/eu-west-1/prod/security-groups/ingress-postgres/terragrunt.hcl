# -------------------------------------------------------
# Allow ingress for postgres
# -------------------------------------------------------
#checkov:skip=CKV2_AWS_5: Attach to wiki-db
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-security-group?ref=v4.3.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

inputs = {
  name = "ingress-postgress"
  description = "Allow incoming on port 5432"
  vpc_id = dependency.vpc.outputs.vpc_id
  ingress_with_cidr_blocks = [
    {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "Allow incoming on port 5432"
      cidr_blocks = join(",", dependency.vpc.outputs.public_subnets_cidr_blocks)
    }
  ]
}