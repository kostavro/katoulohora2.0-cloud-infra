# -------------------------------------------------------
# Allow all outgoing traffic
# -------------------------------------------------------
#checkov:skip=CKV2_AWS_5: Attach to micro-free EC2
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-security-group?ref=v4.3.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

inputs = {
  name = "egress-all"
  description = "Allow all egress"
  vpc_id = dependency.vpc.outputs.vpc_id
  egress_rules = ["all-all"]
}