# -------------------------------------------------------
# Allow HTTPS
# -------------------------------------------------------
#checkov:skip=CKV2_AWS_5: Attach to micro-free EC2
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-security-group?ref=v4.3.0"
}

include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../../vpc/prod-vpc"
}

inputs = {
  name                = "allow-https"
  description         = "Allow HTTPS"
  vpc_id              = dependency.vpc.outputs.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp"]
}