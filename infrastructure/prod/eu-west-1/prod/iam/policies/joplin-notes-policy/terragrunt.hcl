# -------------------------------------------------------
# Policy to allow joplin user to access joplin S3
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-iam//modules/iam-policy?ref=v4.4.0"
}

include {
  path = find_in_parent_folders()
}

dependency "s3_bucket" {
  config_path = "../../../s3/buckets/joplin-notes"
}

inputs = {
  name = "joplin-notes-policy"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "joplin-notes-policy",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Action" : [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:DeleteObject",
            "s3:DeleteObjectVersion",
            "s3:PutObject",
            "s3:PutObjectAcl",
            "s3:GetObjectAcl",
          ],
          "Resource" : [
            "${dependency.s3_bucket.outputs.s3_bucket_arn}",
            "${dependency.s3_bucket.outputs.s3_bucket_arn}/*"
          ]
        },
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Action" : ["s3:ListAllMyBuckets"],
          "Resource" : ["arn:aws:s3:::*",]
        },
        
      ]
    }
  )
}