# -------------------------------------------------------
# Policy to allow assume of admin role
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-iam//modules/iam-policy?ref=v4.4.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = "allow-assume-admin-role"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "allow-assume-admin-role",
      "Statement" : [
        {
          "Sid" : "",
          "Effect" : "Allow",
          "Action" : "sts:AssumeRole",
          "Resource" : "arn:aws:iam::235470632765:role/admin"
        }
      ]
    }
  )
}