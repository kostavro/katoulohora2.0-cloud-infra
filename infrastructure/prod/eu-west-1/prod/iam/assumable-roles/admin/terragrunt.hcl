# -------------------------------------------------------
# Admin role
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-iam//modules/iam-assumable-roles?ref=v4.4.0"
}

include {
  path = find_in_parent_folders()
}

dependency "kostavro" {
  config_path = "../../users/kostavro"
}

inputs = {
  name              = "admin"
  create_admin_role = true
  trusted_role_arns = [dependency.kostavro.outputs.iam_user_arn]
}