# -------------------------------------------------------
# Administrators
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-iam//modules/iam-group-with-policies?ref=v4.6.0"
}

include {
  path = find_in_parent_folders()
}

dependency "admin_role_policy" {
  config_path = "../../policies/allow-assume-admin-role"
}

dependency "joplin_policy" {
  config_path = "../../policies/joplin-notes-policy"
}

dependency "kostavro" {
  config_path = "../../users/kostavro"
}

dependency "eftychia" {
  config_path = "../../users/eftychia"
}

inputs = {
  name = "administrators"
  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess",
    dependency.admin_role_policy.outputs.arn,
    dependency.joplin_policy.outputs.arn
  ]
  group_users = [
    "kostavro",
    "eftychia",
  ]
}