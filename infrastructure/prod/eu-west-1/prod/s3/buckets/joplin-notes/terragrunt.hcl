# -------------------------------------------------------
# S3 storage for Joplin notes
# -------------------------------------------------------
#checkov:skip=CKV_AWS_21: No versioning needed
#checkov:skip=CKV_AWS_144: No cross-region replication
#checkov:skip=CKV2_AWS_6: False-positive for public
# -------------------------------------------------------

terraform {
  source = "git::git@github.com:terraform-aws-modules/terraform-aws-s3-bucket?ref=v2.9.0"
}

include {
  path = find_in_parent_folders()
}

dependency "kms_key" {
  config_path = "../../../kms/joplin-bucket-key"
}

inputs = {
  bucket                  = "katoulohora-joplin-notes"
  attach_public_policy    = true
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  versioning = {
    enabled = false
  }
  server_side_encryption_configuration = {
    rule = {
      apply_server_side_encryption_by_default = {
        kms_master_key_id = dependency.kms_key.outputs.key_arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  attach_policy = true
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "VisualEditor0",
          "Effect" : "Allow",
          "Principal" : "*",
          "Action" : [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:DeleteObject",
            "s3:DeleteObjectVersion",
            "s3:PutObject"
          ],
          "Resource" : [
            "arn:aws:s3:::katoulohora-joplin-notes",
            "arn:aws:s3:::katoulohora-joplin-notes/*"
          ]
        }
      ]
    }
  )
}